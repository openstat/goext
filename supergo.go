package goext

import "log"
import "time"

func SuperGo(label string, fn func(...interface{}), args ...interface{}) {
    go func() {
        for {
            log.Printf("SuperGo call %s", label)
            fn(args...)
            time.Sleep(time.Second)
        }
    }()
}