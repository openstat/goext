Go Extension

Functions

```
goext.SuperGo(label string, fn func(...interface{}), args ...interface{})
```
Infinity loop of calls fn func in separate goroutine and 1 second interval under restart.



Artem Andreenko 
aandreenko@openstat.com