package goext

import "github.com/streadway/amqp"

func AmqpConnect(uri string, queueName string) (amqpConnection *amqp.Connection, amqpChannel *amqp.Channel, err error) {
    amqpConnection, err = amqp.Dial(uri)
    if err == nil {
        amqpChannel, err = amqpConnection.Channel();
    }
    return
}
