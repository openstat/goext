package goext

import "log"

func CheckErr(err error, label string) {
    if err == nil {
        return
    }
    log.Printf("err %s: %s", label, err)
}

func CheckErrFatal(err error, label string) {
    if err == nil {
        return
    }
    log.Printf("err %s: %s", label, err)
}

func PanicRecover(label string) {
    if r := recover(); r != nil {
        var ok bool
        err, ok := r.(error)
        if !ok {
            log.Printf("fatal recover %s: %v, %s", label, r, err)
        }
    }
}